package com.bignerdranch.android.PersonalTaskManager;

import android.support.v4.app.Fragment;

/**
 * Created by Tucker on 25/10/2017.
 */

public class TaskListActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new TaskListFragment();
    }
}
