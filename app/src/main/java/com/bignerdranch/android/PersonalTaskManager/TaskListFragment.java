package com.bignerdranch.android.PersonalTaskManager;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;


import java.util.List;

/**
 * Created by Tucker on 25/10/2017.
 */
public class TaskListFragment extends Fragment {
    private RecyclerView mTaskRecyclerView;
    private TaskAdapter mAdapter;
    private boolean mSubtitleVisible;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_task_list, container, false);

        mTaskRecyclerView = (RecyclerView) view.findViewById(R.id.task_recycler_view);
        mTaskRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        updateUI();
        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.fragment_task_list, menu);
    }

    private void updateUI() {
        TaskLab taskLab = TaskLab.get(getActivity());
        List<Task> tasks = taskLab.getTask();
        if (mAdapter == null) {
            mAdapter = new TaskAdapter(tasks);
            mTaskRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.setTasks(tasks);
            mAdapter.notifyDataSetChanged();
        }
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_task:
                Task task = new Task();
                TaskLab.get(getActivity()).addTask(task);
                Intent intent = MultiPagerActivity
                        .newIntent(getActivity(), task.getId());
                startActivity(intent);
                return true;

        default:
            return super.onOptionsItemSelected(item);    } }








    private class TaskHolder extends RecyclerView.ViewHolder
        implements View.OnClickListener {
        private TextView mTitleTextView;
        private TextView mDateTextView;
        private CheckBox mSolvedCheckBox;
        private Task mTask;

        public void bindTask(Task task) {
            mTask = task;
            mTitleTextView.setText(mTask.getTitle());
            mDateTextView.setText(mTask.getDate().toString());
            mSolvedCheckBox.setChecked(mTask.isSolved());        }

        public TaskHolder(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);

            mTitleTextView = (TextView)
                    itemView.findViewById(R.id.list_item_task_title_text_view);
            mDateTextView = (TextView)
                    itemView.findViewById(R.id.list_item_task_date_text_view);
            mSolvedCheckBox = (CheckBox)
                    itemView.findViewById(R.id.list_item_task_solved_check_box);
        }
        @Override
        public void onClick(View v) {
            Intent intent = MultiPagerActivity.newIntent(getActivity(), mTask.getId());
            startActivity(intent);
        }
    }
        private class TaskAdapter extends RecyclerView.Adapter<TaskHolder> {
            private List<Task> mTasks;

            public TaskAdapter(List<Task> tasks) {

                mTasks = tasks;
            }

            @Override
            public TaskHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
                View view = layoutInflater.inflate(R.layout.list_item_task, parent, false);
                return new TaskHolder(view);
            }

            @Override
            public void onBindViewHolder(TaskHolder holder, int position) {
                Task task = mTasks.get(position);
                holder.bindTask(task);
            }

            @Override
            public int getItemCount() {

                return mTasks.size();
            }
            public void setTasks(List<Task> tasks) {
                mTasks = tasks;
            }
        }
    }
