package com.bignerdranch.android.PersonalTaskManager.databases;

/**
 * Created by Tucker on 26/10/2017.
 */

public class TaskDbSchema {
    public static final class TaskTable {
        public static final String NAME = "Tasks";
        public static final class Cols {
            public static final String UUID = "uuid";
            public static final String TITLE = "title";
            public static final String DATE = "date";
            public static final String SOLVED = "solved";
        }
    }
}
