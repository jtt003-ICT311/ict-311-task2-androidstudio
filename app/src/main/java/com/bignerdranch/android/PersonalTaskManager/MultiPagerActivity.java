package com.bignerdranch.android.PersonalTaskManager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.List;
import java.util.UUID;

/**
 * Created by Tucker on 2/11/2017.
 */

public class MultiPagerActivity extends FragmentActivity {
    private static final String EXTRA_Task_ID =
            "com.bignerdranch.android.PersonalTaskmanager.Task_id";

    private ViewPager mViewPager;
    private List<Task> mTasks;

    public static Intent newIntent(Context packageContext, UUID TaskId) {
        Intent intent = new Intent(packageContext, MultiPagerActivity.class);
        intent.putExtra(EXTRA_Task_ID, TaskId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_pager);

        UUID TaskId = (UUID) getIntent()
                .getSerializableExtra(EXTRA_Task_ID);

        mViewPager = (ViewPager) findViewById(R.id.activity_task_pager_view_pager);
        mTasks = TaskLab.get(this).getTask();
        FragmentManager fragmentManager = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {
                Task task = mTasks.get(position);
                return TaskFragment.newInstance(task.getId());
            }
            @Override
            public int getCount() {
            return mTasks.size();
        }
        });
        for (int i = 0; i < mTasks.size(); i++) {
            if (mTasks.get(i).getId().equals(TaskId)) {
                mViewPager.setCurrentItem(i);
                break;
            }
        }

    }
}
